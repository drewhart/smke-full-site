<?php
/**
 * The Template for displaying all single posts.
 *
 * @package wolf_starter
 */

get_header(); ?>

	<div id="primary" class="content-area site-content eight columns">
			<div id="content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', 'job' ); ?>

		<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>