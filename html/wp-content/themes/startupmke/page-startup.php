<?php
/**
 * Template Name: Startups
 *
 *
 * @package wolf_starter
 */

get_header(); ?>

	<section id="primary" class="content-area site-content tweleve columns">
			<div id="content" role="main">

		<?php /* Loop through Startup Posts*/ ?>
		<?php if ( have_posts() ) : ?>

			<?php echo '<div class="row startup-row">'; 
				$posts_per_row = 3;
				$post_count = 1;
				$max_posts = wp_count_posts ( 'startup' )->publish; // get max number of posts of type 'startup'
			?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'startup' ); ?>

				<?php 
				if ( $post_count == $max_posts ) {
					echo '</div>';
				}
				elseif ( $post_count % $posts_per_row == 0 ) {
					echo '</div><div class="row startup-row">'; 
				}
				
				$post_count++;
				?>
			
			<?php endwhile; ?>

			<?php //wolf_starter_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_footer(); ?>