<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package wolf_starter
 */
?><!DOCTYPE html>
<?php tha_html_before(); ?>
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<?php tha_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php tha_head_bottom(); ?>
<?php wp_head(); ?>
<style>
.site-header .logo img {
padding: 5% 15%;
margin-left: 0;
}
</style>
</head>

<body <?php body_class(); ?>>
<?php tha_body_top(); ?>
<div id="page" class="hfeed site">
	<?php tha_header_before(); ?>
	<?php do_action( 'before' ); ?>
	<div id="head-container">

		<div class="row">
			<header id="masthead" class="site-header" role="banner">
				<?php tha_header_top(); ?>
				<nav id="site-navigation" class="site-navigation main-navigation" role="navigation">
					<h1 class="screen-reader-text"><?php _e( 'Menu', 'wolf_starter' ); ?></h1>
					<div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'wolf_starter' ); ?>"><?php _e( 'Skip to content', 'wolf_starter' ); ?></a></div>
					<div class="row">
						<div class="four columns">
							<?php wp_nav_menu( array( 'theme_location' => 'header-left' ) ); ?>
						</div>
						<div class="four columns logo">
							<a href="<?php bloginfo('url'); ?>"/><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" title="Startup Milwaukee" alt="Startup Milwaukee Logo"></a>
						</div>
						<div class="four columns">
							<?php wp_nav_menu( array( 'theme_location' => 'header-right' ) ); ?>
						</div>
					</div>
				</nav><!-- #site-navigation -->
				<?php tha_header_bottom(); ?>
			</header><!-- #masthead -->
		</div> <!-- row -->

		<?php if(is_home() || is_front_page() ): ?>
		<div class="row">
			<div class="headline twelve columns">
				<div class="home-first-line">
					Discover Milwaukee's
				</div>
				<div class="second-line">
					Growing startup community
				</div>
			</div>
		</div> <!-- row -->
		<!-- email stuffs -->
		<div class="row">
			<div class="header-email-signup twelve columns">
				<?php gravity_form(1, false, false, true); ?>
			</div> <!-- masthead & site-header -->
		</div>
		<!-- end email stuffs -->
		</div> <!-- head-container -->
	<div class="full-width home-hero-bg"></div>
	<?php tha_header_after(); ?>
	<?php else : ?>
			<div class="row">
				<div class="headline twelve columns">
					<div class="first-line">
						Discover Milwaukee's
					</div>
					<div class="second-line">
						Growing startup community
					</div>
				</div>
			</div> <!-- row -->
		</div> <!-- head-container -->
		<div class="full-width hero-bg"></div>
	<?php endif; ?>

<?php tha_header_after(); ?>

<div id="main" class="site-main row">
<?php tha_content_top(); ?>

