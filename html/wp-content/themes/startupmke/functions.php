<?php
/**
 * wolf_starter functions and definitions
 *
 * @package wolf_starter
 */

require ( get_template_directory() . '/inc/htaccess.php' ); // HTML5 Boilerplate .htaccess
require ( get_template_directory() . '/inc/widgets.php' );  // Sidebars and widgets
require ( get_template_directory() . '/inc/scripts.php' );  // Scripts and stylesheets
require ( get_template_directory() . '/inc/tha-hooks.php' ); // Load Theme Hook Alliance files
require ( get_template_directory() . '/inc/jetpack.php' );   // Load Jetpack compatibility file.

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
    $content_width = 640; /* pixels */

if ( ! function_exists( 'wolf_starter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function wolf_starter_setup() {
    require( get_template_directory() . '/inc/template-tags.php' ); // Custom template tags for this theme.
    require( get_template_directory() . '/inc/extras.php' ); // Custom functions that act independently of the theme templates
    require( get_template_directory() . '/inc/customizer.php' ); //Customizer additions

    add_theme_support( 'h5bp-htaccess' ); // Enable HTML5 Boilerplate's .htaccess
    add_theme_support( 'automatic-feed-links' ); //Add default posts and comments RSS feed links to head
    add_theme_support( 'post-thumbnails' ); //Enable support for Post Thumbnails
    add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) ); //Enable support for Post Formats

    register_nav_menus( array(
        'header-left'   => __( 'Header - Left Menu', 'wolf_starter' ),
        'header-right'  => __( 'Header - Right Menu', 'wolf_starter' ),
        'footer'        => __( 'Footer Menu', 'wolf_starter' ),
    ) );

    /**
     * Make theme available for translation
     * Translations can be filed in the /languages/ directory
     * If you're building a theme based on wolf_starter, use a find and replace
     * to change 'wolf_starter' to the name of your theme in all the template files
     */
    load_theme_textdomain( 'wolf_starter', get_template_directory() . '/languages' );

    // Custom images size CIS    
}
endif; // wolf_starter_setup
add_action( 'after_setup_theme', 'wolf_starter_setup' );

///////////////////////
// Custom Post Types
///////////////////////

function st_mke_reg_startup() {

    $labels = array(
        'name'                => _x( 'Startups', 'Post Type General Name', 'startupmke' ),
        'singular_name'       => _x( 'Startup', 'Post Type Singular Name', 'startupmke' ),
        'menu_name'           => __( 'Startup', 'startupmke' ),
        'parent_item_colon'   => __( 'Parent Startup:', 'startupmke' ),
        'all_items'           => __( 'All Startups', 'startupmke' ),
        'view_item'           => __( 'View Startup', 'startupmke' ),
        'add_new_item'        => __( 'Add New Startup', 'startupmke' ),
        'add_new'             => __( 'New Startup', 'startupmke' ),
        'edit_item'           => __( 'Edit Startup', 'startupmke' ),
        'update_item'         => __( 'Update Startup', 'startupmke' ),
        'search_items'        => __( 'Search Startups', 'startupmke' ),
        'not_found'           => __( 'No Startups found', 'startupmke' ),
        'not_found_in_trash'  => __( 'No Startups found in trash', 'startupmke' ),
    );
    $rewrite = array(
        'slug'                => 'startups',
        'with_front'          => true,
        'pages'               => true,
        'feeds'               => true,
    );
    $args = array(
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'page-attributes', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => '',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'             => $rewrite,
        'capability_type'     => 'page',
    );
    register_post_type( 'startup', $args );

}

// Hook into the 'init' action
add_action( 'init', 'st_mke_reg_startup', 0 );

// Job CPT
$job = new Super_Custom_Post_Type( 'job', 'Job', 'Jobs' );
$job->set_icon( 'paper-clip' );

// Mentor CPT
$mentor = new Super_Custom_Post_Type( 'mentor', 'Mentor', 'Mentors' );
$mentor->set_icon( 'bullhorn' );

// End - Custom Post Types

add_image_size( 'logo', 300, 200 );

function sdg_new_excerpt_more( $more ) {
    return ' ... <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Read More</a>';
}
add_filter('excerpt_more', 'sdg_new_excerpt_more');

function sdg_three_posts_on_homepage ( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'posts_per_page', '2' );
    }
    wp_reset_query();
}
add_action( 'pre_get_posts', 'sdg_three_posts_on_homepage');


add_filter ( 'jetpack_sharing_twitter_via', 'sdg_twitter_via' );
 
function sdg_twitter_via() {
    return get_theme_mod( 'smke_twitter', 'startupmke' );
}

function sdg_remove_share_filters(){
    remove_filter( 'the_content', 'sharing_display', 19 );
    remove_filter( 'the_excerpt', 'sharing_display', 19 );
}

add_action( 'init','sdg_remove_share_filters');

function sdg_share_display(){

    echo '<div class="share-wrapper"><div class="share"><span class="plus">+</span>Share</div><div class="share-hidden">';
    if ( function_exists( 'sharing_display' ) ) {
        echo sharing_display();
    }
    echo '</div></div>';
}