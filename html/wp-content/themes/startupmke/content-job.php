<?php
/**
 * @package wolf_starter
 */
?>

<?php
	$job_post_date = DateTime::createFromFormat('Ymd', get_field('job_post_date'));
?>


<?php tha_entry_before(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php tha_entry_top(); ?>
	<header class="entry-header">
	  	<div class="entry-meta"><a href="#"><time class="entry-date"><?php echo $job_post_date->format('F j, Y'); ?></time></a></div>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wolf_starter' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<?php sdg_share_display(); ?>
	</header><!-- .entry-header -->
	
	<div class="entry-content job-posting">
		<?php if(is_single()){ ?>
			<p><?php the_field('job_description'); ?></p>
		<?php edit_post_link( __( 'Edit', 'wolf_starter' ), '<div class="edit-link">', '</div>' ); ?>
		<div class="job-button"><a href="<?php the_field('apply'); ?>">Apply</a></div>
		<?php } ?>
	</div><!-- .entry-summary -->
	<hr>
	<?php tha_entry_bottom(); ?>
</article><!-- #post-## -->
<?php tha_entry_after(); ?>
