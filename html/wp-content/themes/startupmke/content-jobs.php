<?php
/**
 * @package wolf_starter
 */
?>

<?php
	$job_post_date = DateTime::createFromFormat('Ymd', get_field('job_post_date'));
?>


<?php tha_entry_before(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php tha_entry_top(); ?>
	<header class="entry-header">
	  	<div class="entry-meta"><a href="#"><time class="entry-date"><?php echo $job_post_date->format('F j, Y'); ?></time></a></div>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wolf_starter' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	</header><!-- .entry-header -->
	<hr>
	<?php tha_entry_bottom(); ?>
</article><!-- #post-## -->
<?php tha_entry_after(); ?>
