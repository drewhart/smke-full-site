<?php
/*
Template Name: Blog 
*/
get_header(); ?>

	<div id="primary" class="site-content content-area eight columns">
			<div id="content" role="main">

		<?php $query = new WP_Query( array( 'posts_per_page' => 5 ) ); ?>

		<?php if ( $query->have_posts() ) : ?>
		
			<?php /* Start the Loop */ ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>

				
				<?php tha_entry_before(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php tha_entry_top(); ?>
					<header class="entry-header">
						<?php if ( 'post' == get_post_type() ) : ?>
							<div class="entry-meta">
								<?php wolf_starter_posted_on(); ?>
							</div><!-- .entry-meta -->
							<?php endif; ?>
						<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wolf_starter' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					</header><!-- .entry-header -->

					<?php if ( is_search() ) : // Only display Excerpts for Search ?>
					<div class="entry-summary">
						<?php the_excerpt(); // See functions.php for how "Read More" link is rendered ?> 
					</div><!-- .entry-summary -->
					<?php else : ?>
					<div class="entry-content">

						<?php 	the_excerpt(); ?>
						
						<?php 
							wp_link_pages( array( 
								'before' => '<div class="page-links">' . __( 'Pages:', TRUE, 'wolf_starter' ), 
								'after' => '</div>',
							 ) ); 
						?>
					</div><!-- .entry-content -->
					<?php endif; ?>

					<footer class="entry-meta">
							<span class="comment-link">
								
								<?php
								$num_comments = get_comments_number(); // get_comments_number returns only a numeric value
								
								if ( comments_open() ) {
									if ( $num_comments == 0 ) {
										$comments = __('No Comments');
									} elseif ( $num_comments > 1 ) {
										$comments = $num_comments . __(' Comments');
									} else {
										$comments = __('1 Comment');
									}
									$write_comments = '<a href="' . get_comments_link() .'">'. $comments.'</a>';
									echo $write_comments;
								} else {
									$write_comments =  __('Comments are off for this post.');
								} ?>
							
							</span>
							<?php sdg_share_display(); ?>
					</footer><!-- .entry-meta -->
					<?php tha_entry_bottom(); ?>
				</article><!-- #post-## -->
				<?php tha_entry_after(); ?>


			<?php endwhile; ?>

			<?php //wolf_starter_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'index' ); ?>

		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>