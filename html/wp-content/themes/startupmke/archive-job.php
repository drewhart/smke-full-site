<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package wolf_starter
 */

get_header(); ?>
	<section id="primary" class="content-area site-content eight columns">
			<div id="content" role="main">

				<span class="jobs-button widget-jobs post-a-job"><a href="#">Post a Job</a></span>

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'jobs' ); ?>

			<?php endwhile; ?>

			<?php wolf_starter_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->
<?php get_sidebar( 'job' ); ?>
<?php get_footer(); ?>