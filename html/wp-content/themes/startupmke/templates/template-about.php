<?php
/**
 * Template Name: About Page
 *
 * @package wolf_starter
 */

get_header(); ?>

<div id="primary" class="content-area site-content twelve columns about-page">
	<div id="content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php // Headline ?>
			<div class="row">
				<div class="twelve columns">
					<div class="about-headline"><?php echo get_field('headline'); ?></div>
				</div>
			</div>
			<div class="row entry-content">
				<div class="twelve columns">
					<div class="about-statement"><?php echo get_field('about_statement'); ?></div>
				</div>
				<div class="link">
					<a href="mailto:hello@startupmke.org"><i class="icon-envelop button-icon"></i>Email Us</a>
				</div>
			</div>
			<div class="clearfix"></div>

			<?php // Team Members ?>
			<div class="row entry-content">
				<div class="twelve columns">
					<div class="about-headline">Meet the Team</div>
				</div>
			</div>
	  	<div class="row entry-content">
	  		<?php //Advanced Custom Field ?>
				<?php if( get_field('team_members' )) { ?>
	  		<?php $count = 1;	?>
					<?php while(has_sub_field('team_members')) : ?>
			  		<div class="four columns">
							<div class="picture"><img src="<?php echo get_sub_field('picture')['sizes']['logo']; ?>"> </div>
							<div class="name-bio"><?php echo get_sub_field('name_and_bio'); ?> </div>
							<div class="link"><a href="<?php echo get_sub_field('link'); ?>" target="_blank"><i class="icon-book-alt2 button-icon"></i>Learn More</a></div>
						</div>
						<?php if ($count == 3 ){
							echo '</div><div class="row entry-content">';
							$count= 0;
							}
						$count = $count + 1;
					endwhile;
					}	?>
			</div>
			<div class="clearfix"></div>

			<?php // Partners ?>
			<div class="row entry-content">
				<div class="twelve columns">
					<div class="about-headline">Meet Our Partners</div>
				</div>
			</div>
	  	<div class="row entry-content">
	  		<?php //Advanced Custom Field ?>
				<?php	if (get_field('partners')){ ?>
	  		<?php $count = 1;	?>
				<?php while(has_sub_field('partners')) : ?>
						<div class="four columns">
							<div class="partner-picture"><img src="<?php echo get_sub_field('picture')['sizes']['logo']; ?>"> </div>
							<div class="name-bio"><?php echo get_sub_field('description'); ?> </div>
							<div class="link"><a href="<?php echo get_sub_field('link'); ?>" target="_blank"><i class="icon-book-alt2 button-icon"></i>Learn More</a></div>
						</div>
						<?php if ($count == 3 ){
							echo '</div><div class="row entry-content">';
							$count= 0;
							}
						$count = $count + 1;
					endwhile;
					} ?>
				</div>

		<?php endwhile; // end of the loop. ?>
		</div> <!-- entry-content -->
	</div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>
