<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package wolf_starter
 */
?>

<?php tha_entry_before(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php tha_entry_top(); ?>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php the_content(); ?>
		<?php 
			wp_link_pages( array( 
				'before' => '<div class="page-links">' . __( 'Pages:', 'wolf_starter' ), 
				'after' => '</div>',
			) ); 
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
<?php tha_entry_after(); ?>
