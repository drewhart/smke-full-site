<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package wolf_starter
 */
?>
    <?php tha_content_bottom(); ?>
	</div><!-- #main -->
  <?php tha_content_after(); ?>
  <?php tha_footer_before(); ?>
  <div class="full-width footer-follow">
    <div class="row">
      <div class="follow-text six columns centered">
        <p>Follow us</p>
      </div>
    </div>
    <div class="row">
      <div class="follow-links six columns centered">
        <a class="tw" href="http://twitter.com/startupmke"><i class="icon-twitter button-icon"></i>Twitter</a>
        <a class="fb" href="http://facebook.com/startupmke"><i class="icon-facebook button-icon"></i>Facebook</a>
      </div>
    </div>
  </div>
  <div class="full-width footer-email-signup">
    <!-- email stuffs -->
    <div class="row">
      <div class="header-email-signup twelve columns">
        <?php gravity_form(1, false, false, true); ?>
      </div> <!-- masthead & site-header -->
    </div>
    <!-- end email stuffs -->
  </div>
  <div class="full-width footer-bottom">
    <footer id="colophon" class="row site-footer" role="contentinfo">
      <?php tha_footer_top(); ?>
      <div class="eight columns">
        <?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
      </div>
      <div class="four columns">
        <p class="copyright">&copy; <?php echo date( 'Y' ); ?> Startup Milwaukee</p>
<!--        <p class="credits"><a href="<?php echo esc_url( __( 'http://wordpress.org/', 'requiredfoundation' ) ); ?>" target="_blank" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'requiredfoundation' ); ?>" rel="generator"><?php printf( __( 'Powered by %s', 'requiredfoundation' ), 'WordPress' ); ?></a> | <a href="<?php echo esc_url( __( 'http://snowday.io/', 'requiredfoundation' ) ); ?>" target="_blank" title="<?php esc_attr_e( 'Snow Day Group', 'requiredfoundation' ); ?>" rel="generator"><?php printf( __( 'Created by %s', 'requiredfoundation' ), 'Snow Day' ); ?></a></p>

-->
<script type="text/javascript">
      var _gaq=[["_setAccount","UA-39248039-1"],["_trackPageview"]];
      (function(d,t){
        var g=d.createElement(t), s=d.getElementsByTagName(t)[0];
        g.async=1;
        g.src=("https:"==location.protocol?"//ssl":"//www")+".google-analytics.com/ga.js";
        s.parentNode.insertBefore(g,s)
      }(document,"script"));
    </script>

      </div>
      <?php tha_footer_bottom(); ?>
  	</footer><!-- #colophon -->
  </div>
</div><!-- #page -->

<?php tha_footer_after(); ?>
<?php tha_body_bottom(); ?>
<?php wp_footer(); ?>
</body>
</html>