<?php
/**
 * @package wolf_starter
 */
?>

<?php tha_entry_before(); ?>
<div class="four columns">
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  	<?php tha_entry_top(); ?>

    <div class="entry-content">
  		<div class="startup-logo"><?php echo wp_get_attachment_image( get_field( "logo" ), 'logo' ); ?></div>
  		<div class="description"><?php the_field( "description" ); ?></div>
  		<a href="<?php the_field( "link" ); ?>" target="_blank"><div class="startup-button"><i class="icon-book-alt2 button-icon"></i>Learn more</div></a>

  		<?php edit_post_link( __( 'Edit', 'wolf_starter' ), '<div class="edit-link">', '</div>' ); ?>
  	</div><!-- .entry-content -->

  	<?php tha_entry_bottom(); ?>
  </article>
</div>
<?php tha_entry_after(); ?>
