<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package wolf_starter
 */
?>
	<?php tha_sidebars_before(); ?>
	<div id="secondary" class="widget-area four columns" role="complementary">
		<?php tha_sidebar_top(); ?>
		<?php do_action( 'before_sidebar' ); ?>
		<?php dynamic_sidebar( 'sidebar-jobs' ); ?>
		
		<?php tha_sidebar_bottom(); ?>
	</div><!-- #secondary -->
	<?php tha_sidebars_after(); ?>
