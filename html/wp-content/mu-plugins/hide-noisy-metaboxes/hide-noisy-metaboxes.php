<?php 

add_action( 'admin_init', 'hide_noisy_metaboxes' );
function hide_noisy_metaboxes() {
    $hidden = array(
      'dashboard_primary',
      'dashboard_incoming_links', 
      'dashboard_plugins', 
      'dashboard_recent_drafts', 
      'dashboard_secondary',
      'dashboardb_range',
      'tribe_dashboard_widget',
    );
    $current_user = wp_get_current_user();
    update_user_option( $current_user->ID, 'metaboxhidden_dashboard', $hidden, true );
}