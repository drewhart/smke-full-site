<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
// For local development - make sure to add wp-config.local.php to your .gitignore file - this file should NOT be on the production server
if ( file_exists( dirname( __FILE__ ) . '/wp-config.local.php' ) ) {
  include( dirname( __FILE__ ) . '/wp-config.local.php' );
  define( 'WP_LOCAL_DEV', true ); 
} 
else {

  // ** MySQL settings - You can get this info from your web host ** //
  /** The name of the database for WordPress */
  define('DB_NAME', 'startupmke');

  /** MySQL database username */
  define('DB_USER', 'startupmke');

  /** MySQL database password */
  define('DB_PASSWORD', 'enter_a_new_password');

  /** MySQL hostname */
  define('DB_HOST', 'database_url');
  
  define('WP_CACHE', true);


  /**
   * For developers: WordPress debugging mode.
   *
   * Change this to true to enable the display of notices during development.
   * It is strongly recommended that plugin and theme developers use WP_DEBUG
   * in their development environments.
   */
  define('WP_DEBUG', false);

  define('SAVEQUERIES', true);

  /**
   * WordPress Database Table prefix.
   *
   * You can have multiple installations in one database if you give each a unique
   * prefix. Only numbers, letters, and underscores please!
   */
  $table_prefix  = 'wp_startupmke_'; 

}


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@-*/

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

 
define('AUTH_KEY',         '~,Q=Sz4omaw*-5y[z[#oc8$2O =nS%Z+yj8Gpjm7]=;<:*_Le^]}<>Gz#V#:_6`W');
define('SECURE_AUTH_KEY',  'Qcgf|uNe9r:u[Z[Au))9-c2c~IB`Ge|VH>|SsMPvvz|^mPy01rSiB(Q.nA2csL@y');
define('LOGGED_IN_KEY',    'a3T1AXIQ=3$UCza1/[:/mlYm2S[n5-g8k^0~Nh^KrhB21.*kCoCx4tiv{XRBB_fZ');
define('NONCE_KEY',        'DJXpQg|a+&,1|E`%IB5AuvBkw(x}TSY+DgfCU+b&r&uzF^IgVT-|(W=*--<rnKLL');
define('AUTH_SALT',        'sY74*&Avmg6Y1.a[_i_q>>;?E:Nt4nM6A)qVZC6tS-Fa@4CT(A}jYRH5g|@>H+fH');
define('SECURE_AUTH_SALT', '++-X=ttOoNC=WbF#Y9o{7-~?m(}O~C%DaQYjZX8&Rx)VE8-K%aO<upvaI4LQI|rl');
define('LOGGED_IN_SALT',   'AoQSzdf$9iA dAF4fJT0docf<&+dcl2)g}znpR#C{[|x,-z|ig TAL^j/AxfdTV#');
define('NONCE_SALT',       'VmIb([f4S(n%;mk=@pamIs=a7pk+<~Di?`W(Tb/PjYG[7vg^W-9zYp+#%kSqu*oK');


/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
